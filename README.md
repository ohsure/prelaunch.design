Pre-Launch Website Design
=========================

A simple design for a pre-launch website for *Ohsure!* (a new open source course catalog and event registration system). This site's primary focus is the collection of email addresses of individuals interested in being notified of progress reports related to the development of *Ohsure!*.

License
-------

This site design and its assets are licensed under the MIT License. Please see the LICENSE file for more information.
